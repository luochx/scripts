
#==========================================
# THIS SCRIPT DO THE THING WHIT STEP

# 1.apt-get install some tool
# 2.install JDK
# 3.install MAVEN
# 4.install docker-compose
# 5.get jenkins.war and import backup file 
#
#!!!! please change NGINX_URL for use　 
#==========================================
NGINX_URL=reg.csphere.cn

apt-get update
#==========================================
#install curl git 
#==========================================
echo "==============> START apt-get install   <==============  "

#apt-get install -y curl wget git zip unzip tree 
apt-get install -y -qq curl wget git zip unzip tree

echo "========>  installed curl wget git zip unzip tree "

#==========================================
#install docker
#==========================================
echo "==============> START install docker  <==============  "

curl -sSL https://get.docker.com/ | sh

echo "========> docker is installed  "

#==========================================
#install JDK
#==========================================
echo "==============> START install JDK  <==============  "

JAVA_HOME=/usr/lib/jvm/java-1.7-oracle
JRE_HOME=${JAVA_HOME}/jre 
mkdir /home/download
cd /home/download   
curl --silent --location --retry 3 \
http://${NGINX_URL}/jenkins-ci/jdk-7u79-linux-x64.tar.gz \
	| tar xz -C /tmp && \
	mkdir -p /usr/lib/jvm && cp -r /tmp/jdk1.7.0_79/. "${JAVA_HOME}" 


	update-alternatives --install "/usr/bin/java" "java" "${JRE_HOME}/bin/java" 1 && \
	update-alternatives --install "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 1 && \
	update-alternatives --set java "${JRE_HOME}/bin/java" && \
	update-alternatives --set javac "${JAVA_HOME}/bin/javac"

echo "========> JDK is installed  "

#==========================================
#install maven 
#==========================================
echo "==============> START install maven  <==============  "

MAVEN_VERSION=3.3.3 

curl -fsSL http://${NGINX_URL}/jenkins-ci/apache-maven-3.3.3-bin.tar.gz | tar xzf - -C /usr/share && \
 cp  -r /usr/share/apache-maven-3.3.3 /usr/share/maven && \
 ln -s /usr/share/maven/bin/mvn /usr/bin/mvn 
 
echo "========> maven is installed  "


#==========================================
#install maven repository
#==========================================
echo "==============> START install maven repository <==============  "

mkdir -p ~/.m2
# get repository from reg.cpshere.cn
curl -fSL http://${NGINX_URL}/agilefant/repository4agilefant.tar.gz  > ~/.m2/repository4agilefant.tar.gz

# untar 

cd ~/.m2

tar zxvf repository4agilefant.tar.gz 
echo "---------> recovery mvn repository"
 
#==========================================
#install compose 
#==========================================
echo "==============> START install compose  <==============  "

curl -fsSL http://${NGINX_URL}/jenkins-ci/docker-compose > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "========> compose is installed  "

#==========================================
# get jenkins.war 
#==========================================
echo "==============> START get jenkins.war  <==============  "

mkdir /usr/local/jenkins/
curl -fSL http://${NGINX_URL}/jenkins-ci/jenkins.war > /usr/local/jenkins/jenkins.war

echo "========> jenkins.war is geted  "

#==========================================
# rollback jenkins
#==========================================
echo "==============> START import jenkins backup file  <==============  "

JENKINS_BACKUP_PATH=/data/download/jenkins
mkdir -p ${JENKINS_BACKUP_PATH}
curl -fSL http://${NGINX_URL}/jenkins-ci/jenkins-onlyplugin.tar.gz > ${JENKINS_BACKUP_PATH}/jenkins-onlyplugin.tar.gz

tar zxf  ${JENKINS_BACKUP_PATH}/jenkins-onlyplugin.tar.gz -C /root/

echo "========> jenkins plugins has imported  "
